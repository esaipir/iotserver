using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.SignalR;
using APIServerIot.models;
using APIServerIot.hubs;
namespace APIServerIot.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RingApiController : ControllerBase
    {
      
        private readonly ILogger<RingApiController> _logger;
        private readonly IHubContext<RingHub> _hubContext;
        public RingApiController(ILogger<RingApiController> logger,IHubContext<RingHub> hub)
        {
            _logger = logger;
            _hubContext=hub;
         
        }

        [HttpPost]
            [Route("ringingStart")]
        public async Task ringingStart()
        {
            var model=new RingModel();
            model.Id=0;
            model.Ringing=true;
            model.StartAt=DateTime.Now;
            await _hubContext.Clients.All.SendAsync("ringMessage",model);           
        }

        [HttpPost]
        [Route("ringingEnd")]
        public async Task ringingEnd()
        {
            var model=new RingModel();
            model.Id=0;
            model.Ringing=false;
            model.EndAt=DateTime.Now;
            await _hubContext.Clients.All.SendAsync("ringMessage",model);           
        }
    }
}
