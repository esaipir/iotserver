﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using APIServerIot.models;

namespace APIServerIot.hubs
{
    public class RingHub:Hub
    {
        public async Task SendMessage(RingModel model)
        {
            await Clients.All.SendAsync("ringMessage", model);
        } 
    }
}
