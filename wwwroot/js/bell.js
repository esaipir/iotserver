
window.addEventListener("DOMContentLoaded", (event) => {
    const bell = document.querySelector("#bell");
    bell.load("raw/notification-bell-off.json")
    var belltitle=document.querySelector("#bell-text");
    belltitle.innerHTML="Attente de Sonnerie ...";
    beller=document.querySelector(".audio");

    var inter;
    var connection = new signalR.HubConnectionBuilder().withUrl("/ringHub").build();
    connection.on("ringMessage", function (ringModel) {
        if(ringModel.ringing){
            bell.load("raw/58221-notification-bell.json");
            belltitle.innerHTML="Quelqu'un sonne !!!";
            bell.play();
            inter=setInterval(function(){
                beller.play();
            },5000);
        }else{
            bell.load("raw/notification-bell-off.json");
            belltitle.innerHTML="Attente de Sonnerie ...";
            bell.stop();
            clearInterval(inter);


        }
    });
connection.start();
   
  });